﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Respaldo1 {
    public partial class fRespaldo : Form {
        public fRespaldo() {
            InitializeComponent();
        }

        private void bSalir_Click(object sender, EventArgs e) {
            this.Close();
        }

        private void cbTipoRespaldo_SelectedIndexChanged(object sender, EventArgs e) {
            switch (this.cbTipoRespaldo.SelectedIndex) {
                case 0: //Todos Los Usuarios
                    break;
                case 1: //Usuario Actual
                    break;
                case 2: //Usuario Especifico
                    break;
                case 3: //Carpeta Especifica
                    //this.folderBrowserDialog1.RootFolder = Environment.SpecialFolder.UserProfile;
                    this.folderBrowserDialog1.ShowDialog();
                    string ruta = this.folderBrowserDialog1.SelectedPath.ToString();
                    break;
            }
        }

        private void mostrarRuta() {

        }

        private void lbDerechos_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            System.Diagnostics.Process.Start("http://cezaryto.com/");
        }
    }
}
