﻿namespace Respaldo1 {
    partial class fRespaldo {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cbTipoRespaldo = new System.Windows.Forms.ComboBox();
            this.lSeleccion = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbConfiguraciones = new System.Windows.Forms.ComboBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.rtbSalidaCopia = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lbDerechos = new System.Windows.Forms.LinkLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.bSalir = new System.Windows.Forms.Button();
            this.bRespaldar = new System.Windows.Forms.Button();
            this.bGuardar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tipo de Respaldo";
            // 
            // cbTipoRespaldo
            // 
            this.cbTipoRespaldo.FormattingEnabled = true;
            this.cbTipoRespaldo.Items.AddRange(new object[] {
            "Respaldar todos los Usuarios del Sistema",
            "Respaldar Información del Usuario Actual",
            "Respaldo Usuario Específico",
            "Respaldo Carpeta Específica"});
            this.cbTipoRespaldo.Location = new System.Drawing.Point(109, 57);
            this.cbTipoRespaldo.Name = "cbTipoRespaldo";
            this.cbTipoRespaldo.Size = new System.Drawing.Size(215, 21);
            this.cbTipoRespaldo.TabIndex = 1;
            this.cbTipoRespaldo.SelectedIndexChanged += new System.EventHandler(this.cbTipoRespaldo_SelectedIndexChanged);
            // 
            // lSeleccion
            // 
            this.lSeleccion.AutoSize = true;
            this.lSeleccion.Location = new System.Drawing.Point(12, 98);
            this.lSeleccion.Name = "lSeleccion";
            this.lSeleccion.Size = new System.Drawing.Size(123, 13);
            this.lSeleccion.TabIndex = 0;
            this.lSeleccion.Text = "Ubicación Seleccionada";
            this.lSeleccion.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(141, 95);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(331, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cargar Configuración";
            this.label2.Visible = false;
            // 
            // cbConfiguraciones
            // 
            this.cbConfiguraciones.FormattingEnabled = true;
            this.cbConfiguraciones.Location = new System.Drawing.Point(125, 12);
            this.cbConfiguraciones.Name = "cbConfiguraciones";
            this.cbConfiguraciones.Size = new System.Drawing.Size(189, 21);
            this.cbConfiguraciones.TabIndex = 0;
            this.cbConfiguraciones.Visible = false;
            // 
            // rtbSalidaCopia
            // 
            this.rtbSalidaCopia.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.rtbSalidaCopia.Enabled = false;
            this.rtbSalidaCopia.Location = new System.Drawing.Point(12, 191);
            this.rtbSalidaCopia.Name = "rtbSalidaCopia";
            this.rtbSalidaCopia.Size = new System.Drawing.Size(460, 230);
            this.rtbSalidaCopia.TabIndex = 7;
            this.rtbSalidaCopia.Text = "";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 440);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(223, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "Desarrollado por Cesar Casas Quiroga  | México 2017";
            // 
            // lbDerechos
            // 
            this.lbDerechos.AutoSize = true;
            this.lbDerechos.BackColor = System.Drawing.Color.Transparent;
            this.lbDerechos.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDerechos.Location = new System.Drawing.Point(82, 440);
            this.lbDerechos.Name = "lbDerechos";
            this.lbDerechos.Size = new System.Drawing.Size(93, 12);
            this.lbDerechos.TabIndex = 9;
            this.lbDerechos.TabStop = true;
            this.lbDerechos.Text = "Cesar Casas Quiroga";
            this.lbDerechos.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lbDerechos_LinkClicked);
            // 
            // bSalir
            // 
            this.bSalir.Image = global::Respaldo1.Properties.Resources.icons8_Apagar_48;
            this.bSalir.Location = new System.Drawing.Point(416, 10);
            this.bSalir.Name = "bSalir";
            this.bSalir.Size = new System.Drawing.Size(52, 52);
            this.bSalir.TabIndex = 6;
            this.bSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolTip1.SetToolTip(this.bSalir, "Salir");
            this.bSalir.UseVisualStyleBackColor = true;
            this.bSalir.Click += new System.EventHandler(this.bSalir_Click);
            // 
            // bRespaldar
            // 
            this.bRespaldar.Image = global::Respaldo1.Properties.Resources.icons8_Copias_de_seguridad_48;
            this.bRespaldar.Location = new System.Drawing.Point(388, 133);
            this.bRespaldar.Name = "bRespaldar";
            this.bRespaldar.Size = new System.Drawing.Size(52, 52);
            this.bRespaldar.TabIndex = 5;
            this.toolTip1.SetToolTip(this.bRespaldar, "RESPALDAR");
            this.bRespaldar.UseVisualStyleBackColor = true;
            // 
            // bGuardar
            // 
            this.bGuardar.Image = global::Respaldo1.Properties.Resources.icons8_Guardar_48;
            this.bGuardar.Location = new System.Drawing.Point(51, 133);
            this.bGuardar.Name = "bGuardar";
            this.bGuardar.Size = new System.Drawing.Size(52, 52);
            this.bGuardar.TabIndex = 4;
            this.toolTip1.SetToolTip(this.bGuardar, "Guardar");
            this.bGuardar.UseVisualStyleBackColor = true;
            this.bGuardar.Visible = false;
            // 
            // fRespaldo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.ControlBox = false;
            this.Controls.Add(this.lbDerechos);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.bSalir);
            this.Controls.Add(this.rtbSalidaCopia);
            this.Controls.Add(this.cbConfiguraciones);
            this.Controls.Add(this.bRespaldar);
            this.Controls.Add(this.bGuardar);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cbTipoRespaldo);
            this.Controls.Add(this.lSeleccion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "fRespaldo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Respaldo de Información";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbTipoRespaldo;
        private System.Windows.Forms.Label lSeleccion;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button bGuardar;
        private System.Windows.Forms.Button bRespaldar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbConfiguraciones;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.RichTextBox rtbSalidaCopia;
        private System.Windows.Forms.Button bSalir;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel lbDerechos;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

